# Développement Web Fullstack

#### Par Philippe Giraud

## TP PHP Blog 1

**Sujet** : Le sujet du blog va nous accompagner pendant une bonne partie du cours `PHP/MySQL`. Ce TP représente une phase préparatoire. Pour ce TP, vous devez d’abord :

> - Consulter des blogs afin d’en comprendre les principes de fonctionnement.
> - Choisir un sujet qui vous intéresse, identifier 3 blogs le concernant, et choisir celui que vous préférez.

Ensuite, pour préparer le développement :

> - Identifier puis dessiner les grilles (wireframes) de la page d’accueil dans les 3 formats _(desktop, tablette, téléphone)._
> - Fabriquer la page d’accueil en HTML/CSS avec au moins le responsive design pour les 2 formats _**desktop/téléphone**_.
> - Vous vous appuierez au plus sur _**Bootstrap**_ et les _**Flexbox**_.
> - Vous n’utiliserez pas de template pour le moment.
> - Toutes les librairies utilisées seront chargées en CDN.
> - Réutilisez des contenus de votre blog préféré _(urls absolues pour les images)_

**Livraison** : En plus de vos fichiers, vous ajouterez un fichier _**readme.docx**_ dans lequel vous mettrez :

> - Les `Url's` des 3 blogs d’exemple
> - Votre blog préféré
> - Vos 3 grilles dessinées sous forme de photos

**Date limite : 16/06/2019**
